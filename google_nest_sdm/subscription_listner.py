import psycopg2


def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(
            dbname="optiwatt-nest",
            host="localhost",
            user="lukeandresen")

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        print('Inserting')
        cur.execute('INSERT INTO "Optiwatt"."nest_log" ("device_id", "update_type") values (\'3\', \'cold\');')
        conn.commit()
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


if __name__ == '__main__':
    connect()