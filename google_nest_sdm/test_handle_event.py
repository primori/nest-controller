import psycopg2

from google_nest import SubscribeCallback
import json

conn = psycopg2.connect(
    dbname="optiwatt-nest",
    host="localhost",
    user="lukeandresen")

callback = SubscribeCallback(conn=conn)


AMBIENT_TEMP_STR = '{\"eventId\": \"562b7852-a40c-4a4a-9142-19696a789816\", \"timestamp\": \"2021-06-21T15:45:40.397Z\", \"resourceUpdate\": {\"name\": \"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEtnubjq3VeYvk2t5v62ZCaT2h9r33cf0hS14cnLja3ibLAkEitfsqdH2wgGQ3c7c-6nxfFFb4AGHDmoKhvYZAGfRA\", \"traits\": {\"sdm.devices.traits.Temperature\": {\"ambientTemperatureCelsius\": 28.14999}}}, \"userId\": \"AVPHwEtmU5LUq8LZCOUMluFtIibYfNToDd9h1RrQ4uMP\", \"resourceGroup\": [\"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEtnubjq3VeYvk2t5v62ZCaT2h9r33cf0hS14cnLja3ibLAkEitfsqdH2wgGQ3c7c-6nxfFFb4AGHDmoKhvYZAGfRA\"]}'
ambient_temp_obj = json.loads(AMBIENT_TEMP_STR)
callback.handle_event(ambient_temp_obj, True)

DUAL_TEMP_SETPOINT = '{\"eventId\": \"c326a0f4-9174-4163-8800-d50bb776b2c5\", \"timestamp\": \"2021-06-21T15:26:25.225Z\", \"resourceUpdate\": {\"name\": \"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEsZU5SqMz0pI9cy-pu0x4n2oGUwJQMXTfZ5aRVNu0S4K1NPQdT5_e-7jcjSBaQRdn9701TqV2XvkJuo-Y75YbE0xA\", \"traits\": {\"sdm.devices.traits.ThermostatTemperatureSetpoint\": {\"heatCelsius\": 20.19855, \"coolCelsius\": 24.25697}}}, \"userId\": \"AVPHwEtmU5LUq8LZCOUMluFtIibYfNToDd9h1RrQ4uMP\", \"resourceGroup\": [\"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEsZU5SqMz0pI9cy-pu0x4n2oGUwJQMXTfZ5aRVNu0S4K1NPQdT5_e-7jcjSBaQRdn9701TqV2XvkJuo-Y75YbE0xA\"]}'
callback.handle_event(DUAL_TEMP_SETPOINT, False)

HEATCOOL_MODE_SET = '{\"eventId\": \"31f5ddaa-5dc1-4405-90be-8bd87c8baa8c\", \"timestamp\": \"2021-06-21T15:37:00.510Z\", \"resourceUpdate\": {\"name\": \"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEuedgtGRLoEen0gyuz5mMNxUVxShsQOisryERiaY01BRD5etDSAg8tDn8vV24KzaEk6r9diVwA45uJoLqzMLNWiOA\", \"traits\": {\"sdm.devices.traits.ThermostatMode\": {\"mode\": \"HEATCOOL\", \"availableModes\": [\"HEAT\", \"COOL\", \"HEATCOOL\", \"OFF\"]}, \"sdm.devices.traits.ThermostatEco\": {\"availableModes\": [\"OFF\", \"MANUAL_ECO\"], \"mode\": \"OFF\", \"heatCelsius\": 10.16808, \"coolCelsius\": 26.52129}, \"sdm.devices.traits.ThermostatTemperatureSetpoint\": {\"heatCelsius\": 18.99469, \"coolCelsius\": 23.46985}}}, \"userId\": \"AVPHwEtmU5LUq8LZCOUMluFtIibYfNToDd9h1RrQ4uMP\", \"resourceGroup\": [\"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEuedgtGRLoEen0gyuz5mMNxUVxShsQOisryERiaY01BRD5etDSAg8tDn8vV24KzaEk6r9diVwA45uJoLqzMLNWiOA\"]}'
callback.handle_event(HEATCOOL_MODE_SET, False)

AMBIENT_TEMP = '{\"eventId\": \"562b7852-a40c-4a4a-9142-19696a789816\", \"timestamp\": \"2021-06-21T15:45:40.397Z\", \"resourceUpdate\": {\"name\": \"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEtnubjq3VeYvk2t5v62ZCaT2h9r33cf0hS14cnLja3ibLAkEitfsqdH2wgGQ3c7c-6nxfFFb4AGHDmoKhvYZAGfRA\", \"traits\": {\"sdm.devices.traits.Temperature\": {\"ambientTemperatureCelsius\": 28.14999}}}, \"userId\": \"AVPHwEtmU5LUq8LZCOUMluFtIibYfNToDd9h1RrQ4uMP\", \"resourceGroup\": [\"enterprises/4289defb-9175-439e-a6e4-3f8c65ceee93/devices/AVPHwEtnubjq3VeYvk2t5v62ZCaT2h9r33cf0hS14cnLja3ibLAkEitfsqdH2wgGQ3c7c-6nxfFFb4AGHDmoKhvYZAGfRA\"]}'
callback.handle_event(AMBIENT_TEMP, False)