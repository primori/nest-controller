from datetime import datetime
from datetime import timedelta
import json

for i in range(1, 4):
    device_number = i

    all_data = []

    with open('nest' + str(device_number) + '.csv', 'r') as file:
        rows = file.readlines()
        for row in rows:
            row_object = json.loads(row)
            all_data.append(row_object)

    cooling_time = (timedelta(minutes=0, hours=0, seconds=0, microseconds=0))
    heating_time = (timedelta(minutes=0, hours=0, seconds=0, microseconds=0))
    total_time = (timedelta(minutes=0, hours=0, seconds=0, microseconds=0))
    max_time = (timedelta(minutes=0, hours=0, seconds=30, microseconds=0))
    previous_time = 0
    for data in all_data:
        time = datetime.strptime(data[0], '%Y-%m-%dT%H:%M:%S.%f')
        if previous_time != 0:
            time_difference = time - previous_time
            if time_difference < max_time:
                status = data[1]['status']
                if status == 'COOLING':
                    cooling_time += time_difference
                elif status == 'HEATING':
                    heating_time += time_difference
                total_time += time_difference
            else:
                print("Time ignored")

        previous_time = time

    # Found at these links:
    # https://blog.arcadia.com/much-electricity-air-conditioner-uses/
    # https://americanhomewater.com/how-much-power-does-an-air-conditioner-use/
    AC_POWER_RATE_WATT = 3500

    cooling_hours = cooling_time.total_seconds() / 60.0 / 60.0
    cooling_watt_hours = cooling_hours * AC_POWER_RATE_WATT

    print('Device ' + str(device_number) + ' Total Time: ' + str(total_time))
    print('Device ' + str(device_number) + ' Cooling Time: ' + str(cooling_time))
    print('Device ' + str(device_number) + ' Cooling Energy: ' + str(cooling_watt_hours) + ' Watt Hours')
    print('Device ' + str(device_number) + ' Heating Time: ' + str(heating_time))


