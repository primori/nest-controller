from datetime import datetime
from datetime import timedelta
import json
from pathlib import Path


all_data = []
num_file = 1

my_file = Path('nest' + str(num_file) + '.csv')
while my_file.is_file():
    with open(my_file, 'r') as file:
        rows = file.readlines()
        this_file_data = []
        for row in rows:
            row_object = json.loads(row)
            this_file_data.append(row_object)

        all_data.append(this_file_data)
    num_file += 1
    my_file = Path('nest' + str(num_file) + '.csv')


cooling_time = (timedelta(hours=0, minutes=0, seconds=0, microseconds=0))
heating_time = (timedelta(hours=0, minutes=0, seconds=0, microseconds=0))
total_time = (timedelta(hours=0, minutes=0, seconds=0, microseconds=0))
max_time = (timedelta(hours=0, minutes=5, seconds=0, microseconds=0))
previous_time = 0
for i in range(0, len(all_data[0])):
    should_continue = True
    time = datetime.strptime(all_data[0][i][0], '%Y-%m-%dT%H:%M:%S.%f')
    for k in range(0, len(all_data)):
        if all_data[k][i][2]['status'] == 'OFFLINE':
            should_continue = False
    if previous_time != 0 and should_continue:
        time_difference = time - previous_time
        if time_difference < max_time:
            for j in range(0, len(all_data)):
                if all_data[j][i][1]['status'] == 'COOLING':
                    cooling_time += time_difference
                    break
            for j in range(0, len(all_data)):
                if all_data[j][i][1]['status'] == 'HEATING':
                    heating_time += time_difference
                    break
            total_time += time_difference
        else:
            print("Time ignored")
        previous_time = time

print('Time Spent With AC on: ' + str(cooling_time))
print('Time Spent With Heating on: ' + str(heating_time))
