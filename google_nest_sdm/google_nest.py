#!/usr/bin/python3

"""Command line tool for the Google Nest Smart Device Management API.

You must configure your device as described:
  https://developers.google.com/nest/device-access/get-started

which will give you a project_id, client_id, and client_secret.  This tool
will do a one time setup to get an access token, then from there on will
cache the token on local disk.

Once authenticated, you can run commands like:

$ google_nest --project_id=<project_id> list
$ google_nest --project_id=<project_id> get <device_id>
"""
import time
from datetime import datetime
import csv
import sys
from os import path

ROOT_DIR = path.dirname(path.dirname(path.abspath(__file__)))
sys.path.append(ROOT_DIR)

ROOT_DIR = path.dirname(path.abspath(__file__))
sys.path.append(ROOT_DIR)
__package__ = 'google_nest_sdm'

import psycopg2
import argparse
import asyncio
import errno
import json
import logging
import os
import pickle

import yaml
from aiohttp import ClientSession
from google.auth.credentials import Credentials
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow

from .auth import AbstractAuth
from .camera_traits import CameraLiveStreamTrait
from .event import EventMessage
from .google_nest_api import GoogleNestAPI
from .google_nest_subscriber import GoogleNestSubscriber
from .thermostat_traits import (
    ThermostatEcoTrait,
    ThermostatModeTrait,
    ThermostatTemperatureSetpointTrait,
)

conn = None

# Define command line arguments
parser = argparse.ArgumentParser(
    description="Command line tool for Google Nest SDM API"
)
parser.add_argument("--project_id", required=True, help="Device Access program id")
parser.add_argument("--client_id", help="OAuth credentials client_id")
parser.add_argument("--client_secret", help="OAuth credentials client_secret")
parser.add_argument(
    "--token_cache",
    help="File storage for long lived creds",
    default="~/.config/google_nest/token_cache",
)
parser.add_argument(
    "-v", "--verbose", help="Increase output verbosity", action="store_true"
)
parser.add_argument(
    "--output_type",
    type=str,
    choices=["json", "yaml"],
    help="Change the output type from json or yaml (default).",
    default="json",
)

cmd_parser = parser.add_subparsers(dest="command", required=True)
list_structures_parser = cmd_parser.add_parser("list_structures")
list_devices_parser = cmd_parser.add_parser("list_devices")
start_log_parser = cmd_parser.add_parser("start_log")
start_log_parser.add_argument("device_id")
get_structure_parser = cmd_parser.add_parser("get_structure")
get_structure_parser.add_argument("structure_id")
get_device_parser = cmd_parser.add_parser("get_device")
get_device_parser.add_argument("device_id")
set_mode_parser = cmd_parser.add_parser(
    "set_mode", description="Change the thermostat mode."
)
set_mode_parser.add_argument("device_id")
set_mode_parser.add_argument(
    "mode",
    help="The mode to change the thermostat to.",
    choices=["MANUAL_ECO", "HEAT", "COOL", "HEATCOOL", "OFF"],
)
set_heat_parser = cmd_parser.add_parser(
    "set_heat", description="Sets the target temperature when in HEAT mode."
)
set_heat_parser.add_argument("device_id")
set_heat_parser.add_argument("heat", type=float)
set_cool_parser = cmd_parser.add_parser(
    "set_cool", help="Sets the target temperature when in COOL mode."
)
set_cool_parser.add_argument("device_id")
set_cool_parser.add_argument(
    "cool",
    type=float,
    help="The target temperature to set when the thermostat is in COOL mode.",
)
set_range_parser = cmd_parser.add_parser(
    "set_range", help="Sets the min/max temperature when in HEATCOOL mode."
)
set_range_parser.add_argument("device_id")
set_range_parser.add_argument(
    "heat", type=float, help="The minimum target temperature to set."
)
set_range_parser.add_argument(
    "cool", type=float, help="The maximum target temperature to set."
)
generate_rtsp_stream_parser = cmd_parser.add_parser("generate_rtsp_stream")
generate_rtsp_stream_parser.add_argument("device_id")
subscribe_parser = cmd_parser.add_parser("subscribe")
subscribe_parser.add_argument("subscription_id")
subscribe_parser.add_argument("device_id", nargs="?")

OAUTH2_AUTHORIZE = (
    "https://nestservices.google.com/partnerconnections/{project_id}/auth"
)
OAUTH2_TOKEN = "https://www.googleapis.com/oauth2/v4/token"
SDM_SCOPES = [
    "https://www.googleapis.com/auth/sdm.service",
    "https://www.googleapis.com/auth/pubsub",
]
API_URL = "https://smartdevicemanagement.googleapis.com/v1"


class Auth(AbstractAuth):
    """Implementation of AbstractAuth that uses the token cache."""

    def __init__(
            self,
            websession: ClientSession,
            user_creds: Credentials,
            api_url: str,
    ):
        """Initialize Google Nest Device Access auth."""
        super().__init__(websession, api_url)
        self._user_creds = user_creds

    async def async_get_access_token(self):
        """Return a valid access token."""
        return self._user_creds.token

    async def async_get_creds(self):
        """Return valid OAuth creds."""
        return self._user_creds


def CreateCreds(args) -> Credentials:
    """Run an interactive flow to get OAuth creds."""
    creds = None
    token_cache = os.path.expanduser(args.token_cache)
    if os.path.exists(token_cache):
        with open(token_cache, "rb") as token:
            creds = pickle.load(token)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            if not args.client_id or not args.client_secret:
                raise ValueError("Required flag --client_id or --client_secret missing")
            client_config = {
                "installed": {
                    "client_id": args.client_id,
                    "client_secret": args.client_secret,
                    "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob"],
                    "auth_uri": OAUTH2_AUTHORIZE.format(project_id=args.project_id),
                    "token_uri": OAUTH2_TOKEN,
                },
            }
            app_flow = InstalledAppFlow.from_client_config(
                client_config, scopes=SDM_SCOPES
            )
            creds = app_flow.run_console()
        # Save the credentials for the next run
        if not os.path.exists(os.path.dirname(token_cache)):
            try:
                os.makedirs(os.path.dirname(token_cache))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        with open(token_cache, "wb") as token:
            pickle.dump(creds, token)
    return creds


def PrintStructure(structure, output_type):
    """Print the structure."""
    if output_type == "json":
        print(json.dumps(structure.raw_data))
    else:
        print(yaml.dump(structure.raw_data))


def PrintDevice(device, output_type):
    """Print the device."""
    if output_type == "json":
        print(json.dumps(device.raw_data))
    else:
        print(yaml.dump(device.raw_data))


def default(o):
    if type(o) is datetime.date or type(o) is datetime:
        return o.isoformat()


def jsondumps(o):
    return json.dumps(o, default=default)


def PrintDeviceToFile(device, num_device, time_stamp):
    """Print the device."""
    to_print_object = [time_stamp, device.raw_data['traits']['sdm.devices.traits.ThermostatHvac'],
                       device.raw_data['traits']['sdm.devices.traits.Connectivity']]
    with open('nest' + str(num_device) + '.csv', 'a', newline='') as file:
        file.write(jsondumps(to_print_object))
        file.write('\n')


class SubscribeCallback:
    """Print the event message."""

    def __init__(self, output_type=None, conn=None):
        """Initialize SubscribeCallback."""
        self._output_type = output_type
        self._conn = conn

    def handle_event(self, raw_data, should_add_db=True):
        """Handle an EventMessage."""

        print(json.dumps(raw_data))
        self._time_stamp = raw_data['timestamp']
        self._device_id = raw_data['resourceUpdate']['name']
        self._event_id = raw_data['eventId']
        self._should_add_db = should_add_db
        traits = raw_data['resourceUpdate']['traits']

        if 'sdm.devices.traits.Connectivity' in traits:
            status = traits['sdm.devices.traits.Connectivity']['status']
            self.database_helper('conn_status', status=status)

        if 'sdm.devices.traits.ThermostatEco' in traits and traits['sdm.devices.traits.ThermostatEco']['mode'] \
                == 'MANUAL_ECO':

            therm_eco_trait = traits['sdm.devices.traits.ThermostatEco']
            mode = str(therm_eco_trait['mode'])
            heat_temp = str(therm_eco_trait['heatCelsius'])
            cool_temp = str(therm_eco_trait['coolCelsius'])
            self.database_helper('therm_mode', therm_mode=mode)
            self.database_helper('cool_temp', cool_temp=cool_temp)
            self.database_helper('heat_temp', heat_temp=heat_temp)

        else:
            if 'sdm.devices.traits.Temperature' in traits:
                change = str(traits['sdm.devices.traits.Temperature']['ambientTemperatureCelsius'])
                self.database_helper('ambient_temp', temp=change)

            if 'sdm.devices.traits.ThermostatHvac' in traits:
                change = str(traits['sdm.devices.traits.ThermostatHvac']['status'])
                self.database_helper('HVAC_mode', hvac=change)

            if 'sdm.devices.traits.Humidity' in traits:
                change = str(traits['sdm.devices.traits.Humidity']['ambientHumidityPercent'])
                self.database_helper('humidity', humidity=change)

            if 'sdm.devices.traits.ThermostatTemperatureSetpoint' in traits:
                if 'heatCelsius' in traits['sdm.devices.traits.ThermostatTemperatureSetpoint']:
                    change = str(traits['sdm.devices.traits.ThermostatTemperatureSetpoint']['heatCelsius'])
                    self.database_helper('heat_temp', heat_temp=change)
                if 'coolCelsius' in traits['sdm.devices.traits.ThermostatTemperatureSetpoint']:
                    change = str(traits['sdm.devices.traits.ThermostatTemperatureSetpoint']['coolCelsius'])
                    self.database_helper('cool_temp', cool_temp=change)

            if 'sdm.devices.traits.ThermostatMode' in traits:
                change = str(traits['sdm.devices.traits.ThermostatMode']['mode'])
                self.database_helper('therm_mode', therm_mode=change)

            if 'sdm.devices.traits.Fan' in traits:
                change = traits['sdm.devices.traits.Fan']['timerMode']
                self.database_helper('fan_mode', fan_mode=change)

    def database_helper(self, update_type, status='', temp='-1', humidity='-1', cool_temp='-1',
                        heat_temp='-1', hvac='', therm_mode='', fan_mode=''):
        if self._should_add_db:
            cur = self._conn.cursor()
            sql_statement = 'INSERT INTO nest_log ("time_stamp", "device_id", "event_id", "update_type", '\
                            '"conn_status", "temperature", "humidity", "cool_temp", "heat_temp", "hvac", "therm_mode", '\
                            '"fan_mode") '
            sql_values = 'values (\'' + self._time_stamp + '\', \'' + self._device_id + '\', \'' + self._event_id + \
                         '\', \'' + update_type + '\', \'' + status + '\',' + temp + ', ' + humidity + ', ' + \
                         cool_temp + ', ' + heat_temp + ', \'' + hvac + '\', \'' + therm_mode + '\', \'' + fan_mode \
                         + '\');'
            print("executing: " + sql_statement + sql_values)
            cur.execute(sql_statement + sql_values)
            self._conn.commit()

    async def async_handle_event(self, event_message: EventMessage):
        self.handle_event(event_message.raw_data)


class DeviceWatcherCallback:
    """Print the event message."""

    def __init__(self, device, output_type):
        """Initialize DeviceWatcherCallback."""
        self._device = device
        self._output_type = output_type

    async def async_handle_event(self, event_message: EventMessage):
        """Handle an EventMessage."""
        print(f"event_id: {event_message.event_id}")
        print("Current device state:")
        PrintDevice(self._device, self._output_type)
        print("")


async def RunTool(args, user_creds: Credentials):
    """Run the command."""
    async with ClientSession() as client:
        auth = Auth(client, user_creds, API_URL)
        api = GoogleNestAPI(auth, args.project_id)

        if args.command == "list_structures":
            structures = await api.async_get_structures()
            for structure in structures:
                PrintStructure(structure, args.output_type)
            return

        if args.command == "get_structure":
            structure = await api.async_get_structure(args.structure_id)
            PrintStructure(structure, args.output_type)
            return

        if args.command == "list_devices":
            devices = await api.async_get_devices()
            for device in devices:
                PrintDevice(device, args.output_type)
            return

        if args.command == "subscribe":
            logging.info("Subscription: %s", args.subscription_id)
            subscriber = GoogleNestSubscriber(
                auth, args.project_id, args.subscription_id
            )
            if args.device_id:
                device_manager = await subscriber.async_get_device_manager()
                device = device_manager.devices[args.device_id]
                callback = DeviceWatcherCallback(device, args.output_type)
                device.add_event_callback(callback.async_handle_event)
            else:
                conn = psycopg2.connect(
                    dbname="postgres",
                    host="nest-controller-db.cljesubrlm1x.us-east-2.rds.amazonaws.com",
                    user="postgres",
                    password="ComalStreet"
                         )
                callback = SubscribeCallback(args.output_type, conn)
                subscriber.set_update_callback(callback.async_handle_event)
            await subscriber.start_async()
            try:
                while True:
                    await asyncio.sleep(10)
            except KeyboardInterrupt:
                subscriber.stop_async()

        # All other commands require a device_id
        device = await api.async_get_device(args.device_id)

        if args.command == "get_device":
            PrintDevice(device, args.output_type)

        if args.command == "set_mode":
            mode = args.mode
            trait = device.traits[ThermostatModeTrait.NAME]
            if mode == "MANUAL_ECO":
                trait = device.traits[ThermostatEcoTrait.NAME]
            resp = await trait.set_mode(mode)
            print(await resp.text())

        if args.command == "set_heat":
            trait = device.traits[ThermostatTemperatureSetpointTrait.NAME]
            resp = await trait.set_heat(args.heat)
            print(await resp.text())

        if args.command == "set_cool":
            trait = device.traits[ThermostatTemperatureSetpointTrait.NAME]
            resp = await trait.set_cool(args.cool)
            print(await resp.text())

        if args.command == "set_range":
            trait = device.traits[ThermostatTemperatureSetpointTrait.NAME]
            resp = await trait.set_range(args.heat, args.cool)
            print(await resp.text())

        if args.command == "generate_rtsp_stream":
            trait = device.traits[CameraLiveStreamTrait.NAME]
            stream = await trait.generate_rtsp_stream()
            print(f"URL: {stream.rtsp_stream_url}")
            print(f"Stream Token: {stream.stream_token}")
            print(f"Expires At: {stream.expires_at}")

        if args.command == "start_log":

            while True:
                # TODO: Convert this to use updates method rather than polling
                devices = await api.async_get_devices()
                cur_time = datetime.now()
                i = 1
                for device in devices:
                    if device.type == 'sdm.devices.types.THERMOSTAT':
                        PrintDeviceToFile(device, i, cur_time)
                        i = i + 1
                # Anything less than 37 will exceed the hourly query limit
                # https://developers.google.com/nest/device-access/project/limits
                time.sleep(37)


def main():
    """Nest command line tool."""
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    user_creds = CreateCreds(args)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(RunTool(args, user_creds))
    loop.close()


if __name__ == "__main__":
    main()
