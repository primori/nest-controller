# Luke Andresen's Modifications:

1. Passing start_log as the function in the configuration when running google_nest.py 
creates a file for each nest if they don't already exist, and logs 
the status of each nest in these files.
2. analyze_nest_usage finds the amount of time each thermostat is heating or cooling.
3. nest_comparison finds the total amount of time a HVAC was on.

# Running on Ubuntu:

To connect: ssh -i "Nest_Controller_Key.pem" ubuntu@ec2-18-222-112-66.us-east-2.compute.amazonaws.com
1. cd into google_nest_sdm
2. Run sudo apt update
3. Run sudo apt install python3-pip
4. Run git pull origin
5. Run pip install -r requirements2.txt
6. To start Sub, run python3 -m google_nest --project_id 4289defb-9175-439e-a6e4-3f8c65ceee93 --client_id 355171177424-6dh2guieb2qv1o45de8uei2m4utps1vg.apps.googleusercontent.com --client_secret yhv3co0EYpHA4Z7TlQjcwDuc subscribe projects/arcane-attic-315715/subscriptions/nest_sub

# python-google-nest-sdm

This is a library for Google Nest [Device Access](https://developers.google.com/nest/device-access)
using the [Smart Device Management API](https://developers.google.com/nest/device-access/api).

# Usage

This can be used with the sandbox which requires [Registration](https://developers.google.com/nest/device-access/registration), accepting terms
and a fee.

You'll want to following the [Get Started](https://developers.google.com/nest/device-access/get-started)
guides for setup including steps in the google cloud console.  Overall, this is
fairly complicated with many steps that are easy to get wrong.  It is likely
worth it to make sure you can get the API working using their supplied curl
commands with your account before attempting to use this library.

# Structure

This API was designed for use in Home Assistant following the advice in
[Building a Python Library for an API](https://developers.home-assistant.io/docs/api_lib_index/).

If you are integrating this from outside Home Assistant, you'll need to
create your own oauth integration and token refresh mechanism and tooling.

# Fetching Data

This is an example to use the command line tool to access the API:

```
PROJECT_ID="some-project-id"
CLIENT_ID="some-client-id"
CLIENT_SECRET="some-client-secret"
# Initial call will ask you to authorize OAuth2 then cache the token
google_nest --project_id="${PROJECT_ID}" --client_id="${CLIENT_ID}" --client_secret="${CLIENT_SECRET}" list_structures
# Subsequent calls only need the project id
google_nest --project_id="${PROJECT_ID}" get_device "some-device-id"
google_nest --project_id="${PROJECT_ID}" set_mode COOL
google_nest --project_id="${PROJECT_ID}" set_cool 25.0
```

# Subscriptions

See [Device Access: Getting Started: Subscribe to Events](https://developers.google.com/nest/device-access/subscribe-to-events)
for documentation on how to create a pull subscription.

You can create the subscription to use with the tool with these steps:

* Create the topic:
  * Visit the [Device Access Console](https://console.nest.google.com/device-access)
  * Select a project
  * Enable Pub/Sub and note the full `topic` based on the `project_id`
* Create the subscriber:
  * Visit [Google Cloud Platform: Pub/Sub: Subscriptions](https://console.cloud.google.com/cloudpubsub/subscriptions)
  * Create a subscriber
  * Enter the `Topic Name`
  * Create a `Subscription Name`, e.g. "project-id-python" which is your `subscriber_id`

This is an example to run the command line tool to subscribe:
```
PROJECT_ID="some-project-id"
SUBSCRIPTION_ID="projects/some-id/subscriptions/enterprise-some-project-id-python-google-nest"
google_nest --project_id="${PROJECT_ID}" subscribe ${SUBSCRIPTION_ID}
```

# Development

```
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -e .
$ pip3 install -r requirements.txt

# Running tests
$ pytest

# Formatting
$ isort tests/*.py google_nest_sdm/*.py
$ black tests/*.py google_nest_sdm/*.py
$ flake8 tests/*.py google_nest_sdm/*.py

# Releasing
$ python3 setup.py sdist bdist_wheel
$ twine upload --skip-existing dist/*
```

# Funding and Support

If you are interested in donating money to this effort, instead send a
donation to [Black Girls Code](https://donorbox.org/support-black-girls-code)
which is a great organization growing the next generation of software engineers.
